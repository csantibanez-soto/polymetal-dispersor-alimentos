/*
 * TITLE: POLYMETAL - DISPERSOR DE ALIMENTOS IOT
 * AUTHOR: CHRISTIAN SANTIBÁÑEZ SOTO
 * COMPANY: LEUFÜLAB
 * VERSION: 1.1 BUGFIX: NODE ID
 * DATE: 22/04/2021  
 */

// LIBRARIES
#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <LowPower.h>


// IO DECLARATION
const int hall_sgn = 8;
const int hall_pwr = 9;

const lmic_pinmap lmic_pins = {
    .nss = 6,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 5,
    .dio = {2, 3, LMIC_UNUSED_PIN},
};

// VARIABLES
bool hall_state = false;
bool last_hall_state = false;

bool rotating = false;
unsigned long rot_time_0, rot_time_1;
unsigned long delta_rot_time = 0;
int j = 0;

int sum = 0;
int average_time = 0;
int i = 0;
int rpm = 0; 
unsigned long hall_time_0, hall_time_1, delta_hall_time, timeout_0, timeout_1, delta_timeout;

//// LORAWAN DATA
////ABP
static const PROGMEM u1_t NWKSKEY[16] = { 0x2A, 0x54, 0x8C, 0xD5, 0x2C, 0x35, 0x4F, 0xD2, 0x99, 0xE5, 0xFD, 0xBC, 0x75, 0x3D, 0x04, 0x8F };
static const u1_t PROGMEM APPSKEY[16] = { 0x3D, 0x27, 0xA3, 0xCF, 0xD2, 0x64, 0xA6, 0xFA, 0x63, 0xC2, 0xC9, 0xB5, 0x2C, 0xDC, 0x8E, 0x68 };
static const u4_t DEVADDR = 0x260218B3 ; // <-- Change this address for every node!
u4_t node_id = 1; // <-- Número de identificación del nodo
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

static osjob_t sendjob;
static u4_t frame_counter = 0; 
const unsigned TX_INTERVAL = 128; // 758 para 15 minutos (Teórico)256 para 5 minutos

void setup() {
  ADCSRA = 0; // TURN-OFF THE ADC
  Serial.begin(115200); //SERIAL DEBUGGING
  Serial.println(F("-- STARTING --"));
 
  pinMode(hall_sgn, INPUT);
  pinMode(hall_pwr, OUTPUT);

  session_configuration();
}

void loop() {
  os_runloop_once();
//  rotation_validation();
//  if(rotating){
//    rpm_calculation();
////    digitalWrite(hall_pwr, LOW);
//  }
//  else{
//    rpm = 0;
////    digitalWrite(hall_pwr, LOW);
//  }
//  delay(10000);
}

void session_configuration(){
    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();
    

    // Set static session parameters. Instead of dynamically establishing a session
    // by joining the network, precomputed session parameters are be provided.
    #ifdef PROGMEM
    // On AVR, these values are stored in flash and only copied to RAM
    // once. Copy them to a temporary buffer here, LMIC_setSession will
    // copy them into a buffer of its own again.
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
//    LMIC_setSession (0x13, DEVADDR, nwkskey, appskey);
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
    // If not running an AVR with PROGMEM, just use the arrays directly
//    LMIC_setSession (0x13, DEVADDR, NWKSKEY, APPSKEY);
    LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif
    
    #if defined(CFG_us915) || defined(CFG_au915)
    // NA-US and AU channels 0-71 are configured automatically
    // but only one group of 8 should (a subband) should be active
    // TTN recommends the second sub band, 1 in a zero based count.
    // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
    LMIC_selectSubBand(1);


    #else
    # error Region not supported
    #endif

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink
    LMIC_setDrTxpow(DR_SF7,14);

    //Set RFM9X Frame Counter
    LMIC.seqnoUp = frame_counter;

    // Start job
    do_send(&sendjob);
}
