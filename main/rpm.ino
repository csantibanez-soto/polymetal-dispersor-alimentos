void rotation_validation(){
  delta_rot_time = 0;
//  rotating = false; // CH: 24052021
  rot_time_0 = millis();
  digitalWrite(hall_pwr, HIGH);
  delay(100);
  
  while((rotating == false) && (delta_rot_time < 3000)){
    
    hall_state = digitalRead(hall_sgn);
    if(hall_state != last_hall_state){
      j++;
    }
    if(j > 2){
      rotating = true;
    }
    
    rot_time_1 = millis();
    delta_rot_time = rot_time_1 - rot_time_0;
    last_hall_state = hall_state;
  }
  j = 0;
}

void rpm_calculation(){
  i = 0;
  sum = 0;
  rotating = false;

  hall_time_0 = millis();
  last_hall_state = digitalRead(hall_sgn);
  delta_timeout = 0;
  
  timeout_0 = millis();
  while((i < 11) && (delta_timeout < 7000)){
    
    hall_state = digitalRead(hall_sgn);
    
    if(hall_state != last_hall_state){
      hall_time_1 = millis();
      delta_hall_time = hall_time_1 - hall_time_0;
      if(i == 0){
        delta_hall_time = 0;
      }
      sum = sum + delta_hall_time;
      i++;
    }
    timeout_1 = millis();
    delta_timeout = timeout_1 - timeout_0;
    last_hall_state = hall_state;
    hall_time_0 = hall_time_1;
  }

  average_time = sum*0.1;
  rpm = 15000/average_time;
  delta_hall_time = 0;

  if(delta_timeout >= 7000){
    rpm = 0;
  }
}
